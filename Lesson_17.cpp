﻿// Lesson_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>
using namespace std;

class my_class
{

private:
	int a, b, c;
	
public:
	my_class() :a(0), b(0), c(0)
	{
	}
	my_class(int _x,int _y,int _z)
	{
		a = _x;
		b = _y;
		c = _z;
	}
	void PrintVector()
	{
		cout << "x= " << a << " y= " << b << " z= " << b<<endl;
	}
	double ModVector()
	{
		return sqrt(a * a + b * b + c * c);
	}
};

int main()
{
	setlocale(LC_ALL, "ru");

	my_class MyVector1(2,3,2),MyVector2(5,4,3);
	MyVector1.PrintVector();
	cout << "Модуль вектора= "<<MyVector1.ModVector()<<endl;

	MyVector2.PrintVector();
	cout <<"Модуль вектора= "<< MyVector2.ModVector()<<endl;
	
	return 0;

}